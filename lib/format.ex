defmodule Loggy.Format do
  @moduledoc """
  Default formatting functions of loggy. They should take a string and return something printable.
  This can be a string, or a list, as is the case with `IO.ANSI.format/2`, used for colour in the
  default functions.

  There should, generally, be no reason for you, the end user, to directly call these functions.
  Replacing them, however, might be in your interest. You'll do this by putting functions in
  the respective default options, or if you'd prefer, passing them manually with each call.

  ## Examples

      iex> Loggy.set_fn_params([
      ...>   debug: [format: fn str -> "[DEBG] " <> str end],
      ...>   info: [format: fn str -> "[INFO] " <> str end],
      ...>   warn: [format: fn str -> "[WARN] " <> str end],
      ...>   error: [format: fn str -> "[ERRO] " <> str end]
      ...> ])
      iex> Loggy.info("Hello!")
      #=> [INFO] Hello!
      :ok

  """

  @doc """
  Default formatting method for debug messages.
  To replace, write a function that takes and returns a string and
  assign `fn_params[:debug][:format]` to it.
  """
  @spec debug(String.t()) :: IO.chardata() | String.Chars.t()
  def debug(str) do
    IO.ANSI.format(
      [:green, "🐛 ", :reset, str],
      Loggy.get_user_opts()[:color]
    )
  end

  @doc """
  Default formatting method for info messages.
  To replace, write a function that takes and returns a
  printable item and assign `fn_params[:format][:info]` to it.
  """
  @spec info(String.t()) :: IO.chardata() | String.Chars.t()
  def info(str) do
    IO.ANSI.format(
      [:cyan, " ℹ️ ", :reset, str],
      Loggy.get_user_opts()[:color]
    )
  end

  @doc """
  Default formatting method for warning messages.
  To replace, write a function that takes and returns a string and
  assign `fn_params[:format][:warn]` to it.
  """
  @spec warn(String.t()) :: IO.chardata() | String.Chars.t()
  def warn(str) do
    IO.ANSI.format(
      [:yellow, " ⚠️ ", :reset, str],
      Loggy.get_user_opts()[:color]
    )
  end

  @doc """
  Default formatting method for debug strings.
  To replace, write a function that takes and returns a string and
  assign `fn_params[:format][:error]` to it.
  """
  @spec error(String.t()) :: IO.chardata() | String.Chars.t()
  def error(str) do
    IO.ANSI.format(
      [:red, "🚫 ", :reset, str],
      Loggy.get_user_opts()[:color]
    )
  end
end
