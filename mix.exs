defmodule Loggy.MixProject do
  use Mix.Project

  def project do
    [
      app: :loggy,
      version: "0.1.1",
      elixir: "~> 1.8",
      description: "A simple logging utility built for CLIs",
      deps: deps(),
      package: package(),
      application: application()
    ]
  end

  def application() do
    [
      mod: {Loggy, []},
      registered: [Loggy]
    ]
  end

  defp deps() do
    [
      {:ex_doc, "~> 0.19.3", only: :dev}
    ]
  end

  defp package() do
    [
      files: ["lib", "test", "mix.exs", "README.md", "LICENSE", ".gitlab-ci.yml"],
      maintainers: ["Jorin S."],
      licenses: ["BSD 2-clause"],
      links: %{
        "GitLab" => "https://gitlab.com/jorins/loggy"
      },
      source_url: "https://gitlab.com/jorins/loggy"
    ]
  end
end
