# Loggy
Loggy is a simple logging tool designed for Elixir CLIs, replacing the
default Logger. Loggy does **not** integrate with Logger.  Instead, it's
tailored around CLI usage.

Loggy supports configuration through direct passthrough of parsed arguments from
OptionParser. `verbose`, `debug`, and `color`/`colour` are supported. `verbose`
supports both a binary flag and multiple levels.

Loggy also uses colour straight out of the box. This tends to be a bit trickier
with Logger


## Installation
Just add to your dependencies:

```elixir
def deps do
  [
    {:loggy, "~> 0.1.0"}
  ]
end
```


## Usage
Read the documentation at [hexdocs.pm](https://hexdocs.pm/loggy)


## Contributing
* Fork
* Write code
* Update documentation and doctests
* `mix test` to make sure it looks like it works
* `mix format` to automatically format the code (otherwise it won't pass in the CI)
* Submit a pull request

Loggy is not a high effort project and the code-base is very simple, so don't
be afraid to participate if you see the need for a change. Everything is
appreciated!
