defmodule LoggyTest do
  use ExUnit.Case
  doctest Loggy
  doctest Loggy.Format

  # TODO: since we can't capture IO in doctests,
  # there should be some manual tests here. Worthwhile tests:
  # standard functionality
  # changing and resetting settings
  # passing from OptionParser
end
